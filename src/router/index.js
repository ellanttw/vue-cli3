import Vue from 'vue'
import Router from 'vue-router'
import AppConten from '@/pages/layout/AppConten'
import Home from '@/pages/home'
import Detail from '@/pages/detail'
import list from '@/pages/list'
import Login from '@/pages/login'
import NotFound from '@/pages/NotFound'

Vue.use(Router)

export default new Router({
  mode: 'history',
  history: true,
  routes: [
    {
      path: '/',
      component: AppConten,
      children: [
        {
          path: '/',
          name: 'Home',
          components: {
            default: Home
          }
        },
        {
          path: 'list',
          name: 'list',
          components: {
            default: list
          }
        },
        {
          path: 'login',
          name: 'Login',
          components: {
            default: Login
          }
        },
        {
          path: 'page::id',
          name: 'Detail',
          components: {
            default: Detail
          }
        }
      ]
    },
    {
      path: '/*',
      component: NotFound,
    }
  ]
})
