
import axios from 'axios'

export const MixinsBlog = {
    methods: {
        getDataDumi: function () {
            this.$store.commit('auth_request')
            return axios.get(process.env.VUE_APP_API_DATADUMI)
        },
        getDetailDumi: function (numb) {
            this.$store.commit('auth_request')
            return axios.get(process.env.VUE_APP_API_DATADUMI + numb)
        }
    }
  }
  